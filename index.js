const app = require('../util');

const port = 3000;

// Definir o uso das rotas da API de Mock
app.use('*', app.routes.requestRoutes);

const analyzeRequest = (req, res, next) => {
  // Realize a análise da requisição aqui
  app.logger.log('Requisição recebida:', req.method, req.url, req.body);

  // Chame o próximo middleware ou manipulador de rota
  next();
};

app.use(analyzeRequest);

// Iniciar o servidor
app.listen(port, () => {
  console.log(`API de Mock rodando em http://localhost:${port}`);
});
